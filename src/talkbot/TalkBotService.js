import{NearbyPlaces, LocationAddress, FindPlace} from '../api/MapsService.js';
import{InsertUserLocation} from '../service/UserService.js';

import{StatusText, Speak} from '../traffic_lights.js';

import {
    InitRoute
} from '../api/MapBoxService.js';

import RouteService from '..//service/RouteService.js';

import { Platform } from 'react-native';

class TalkBotService
{
    Tts = null;
    isInQuestion = false;
    isInConfirmation = false;
    isInSpeech = false;
    Voice = null;
    applicationState = null;
    location = null;
    initDelay = 3000;
    continueDelay = 150;

    questionInfo = {
        callback: null,
        question: null,
        confirmationQuestion: null,
        isConfirmed: false,
        isConfirming: false,
        response: null
    }

    static instance = null;

    static getInstance() {

        if (!TalkBotService.instance) {

            TalkBotService.instance = new TalkBotService();

        }
        return TalkBotService.instance;
    }

    /**
     * Initialize the talkbot service.
     * @param {*} Tts 
     * @param {*} Voice 
     * @param {*} applicationState 
     */
    register = (Tts, Voice, applicationState) =>
    {
        this.Tts = Tts;
        this.Voice = Voice;

        this.Voice.onSpeechStart = this.onSpeechStart;
        this.Voice.onSpeechRecognized = this.onSpeechRecognized;
        this.Voice.onSpeechEnd = this.onSpeechEnd;
        this.Voice.onSpeechError = this.onSpeechError;
        this.Voice.onSpeechResults = this.onSpeechResults;
        this.Voice.onSpeechPartialResults = this.onSpeechPartialResults;
        
        this.applicationState = applicationState;

        Tts.addEventListener('tts-finish', this.onTtsFinish);
        Tts.addEventListener('tts-start', this.onTtsStart);
        
    }

    setLocation = (location) => 
    {
        this.location = location;
    }

    //TTS:
    /**
     * Called when the TTS is finished.
     * @param {event} event 
     */
    onTtsFinish = (event) => {
        isInSpeech = false;
        if(this.isInQuestion || this.isInConfirmation)
        {
            console.log('esperando resposta...');
            this.StartRecognizing();
        }
    };

    /**
     * Called when TTS is starting.
     * @param {event} event 
     */
    onTtsStart = (event) => {

        console.log('tts start...');
        isInSpeech = true;
    };

    //VOICE:
    onSpeechStart = (e) => {
        console.log('onSpeechStart: ', e);

        if(Platform.OS == 'ios')
        {
            console.log('set timeout IOS');
            timeout = setTimeout(handleTimeout, InitDelay);
        }
    };

    onSpeechPartialResults = (e) =>{
        //console.log('onSpeechPartialResults: ', e);
        if(Platform.OS == 'ios')
        {
            if(timeout) { 
                clearTimeout(timeout);
            }
            timeout = setTimeout(StopRecognizing, ContinueDelay);
        }
    };

    onSpeechRecognized = (e) => {
        console.log('onSpeechRecognized: ', e);
    };

    onSpeechEnd = (e) => {
        console.log('onSpeechEnd: ', e);
    };

    onSpeechError = (e) => {
        console.log('onSpeechError: ', e);

        if(this.isInQuestion || this.isInConfirmation)
        {
            Speak('Desculpe, não entendi ' + this.GetQuestionText(), this.Tts);
        }
    };

    /**
     * Called when the speech is finished.
     * @param {event} e 
     */
    onSpeechResults = e => {
        console.log('onSpeechResults: ', e);
        this.ProcessText(e.value, this.Tts);
    };

    /**
     * Starts the voice recognition.
     */
    StartRecognizing = async () => {
        try {
            if(!this.isInSpeech)
            {
                await this.Voice.start('pt-BR');
            }
        } catch (e) {
            console.error(e);
        }
    };

    /**
     * Stops the voice recognition.
     */
    StopRecognizing = async () => {
        try {
            await this.Voice.stop();
        } catch (e) {
            console.error(e);
        }
    };

    /**
     * Process the words spoken by the user.
     * @param {string[]} words 
     */
    ProcessText = (words) =>{

        if(this.isInQuestion) 
        {
            this.ProcessQuestion(words);
        }
        else if(this.isInConfirmation)
        {
            this.ProcessConfirmation(words);
        }
        else //Process as command
        {
            this.RecognizeCommand(words);
        }
    }

    /**
     * Process the confirmation.
     * @param {string[]} words 
     */
    ProcessConfirmation = (words) => {
        if(!this.questionInfo.isConfirmed)
        {
            if(words.includes('sim'))
                {
                    if(this.questionInfo.callback != null)
                    {
                        this.questionInfo.callback(...this.questionInfo.params);
                    }

                    questionInfo = {
                        callback: null,
                        question: null,
                        confirmationQuestion: null,
                        isConfirmed: false,
                        isConfirming: false,
                        response: null,
                        params: null
                    }

                    this.isInConfirmation = false;
                }
                else if (words.includes('não'))
                {
                    //does nothing
                    Speak('Ação cancelada.', this.Tts);

                    questionInfo = {
                        callback: null,
                        question: null,
                        confirmationQuestion: null,
                        isConfirmed: false,
                        isConfirming: false,
                        response: null,
                        params: null
                    }

                    this.isInConfirmation = false;
                }
                else
                {
                    Speak('Desculpe, não entendi.', this.Tts);
                }
        }
    }

    /**
     * Process the question.
     * @param {string[]} words 
     */
    ProcessQuestion = (words) => {
        if(!this.questionInfo.isConfirmed)
        {
            if(!this.questionInfo.isConfirming)
            {
                this.questionInfo.response = words[0];
                Speak(this.questionInfo.confirmationQuestion + '' + words[0], this.Tts);

                this.questionInfo.isConfirming = true;
            }
            else
            {
                if(words.includes('sim'))
                {
                    if(this.questionInfo.callback != null)
                    {
                        this.questionInfo.callback(this.questionInfo.response, ...this.questionInfo.params);
                    }

                    questionInfo = {
                        callback: null,
                        question: null,
                        confirmationQuestion: null,
                        isConfirmed: false,
                        isConfirming: false,
                        response: null,
                        params: null
                    }

                    this.isInQuestion = false;
                }
                else if (words.includes('não'))
                {
                    this.questionInfo.isConfirming = false;
                    Speak(this.questionInfo.question, this.Tts);
                }
                else
                {
                    Speak('Desculpe, não entendi', this.Tts);
                }
            }
        }
    }

    /**
     * Gets the current text for the question.
     */
    GetQuestionText = () =>{
        if(this.isInQuestion)
        {
            if(this.questionInfo.isConfirming)
            {
                return this.questionInfo.confirmationQuestion + ' ' + this.questionInfo.response;
            }
            return this.questionInfo.question;
        }
        else if(this.isInConfirmation)
        {
            return this.questionInfo.question;
        }
    }

    /**
     * Asks a question given the parameters.
     * @param {string} questionText Question text
     * @param {string} confirmationQuestionText Confirmation text for the question
     * @param {function} callback Callback function called when the response is ok.
     * @param {param} params Parameters to be aplied to the callback when the response is ok.
     */
    AskQuestion = (questionText,confirmationQuestionText, callback, ...params) => {
        
        if(!this.isInQuestion)
        {
            this.questionInfo = {
                callback: callback,
                question: questionText,
                confirmationQuestion: confirmationQuestionText,
                isConfirmed: false,
                params: params
            }
            
            this.isInQuestion = true;

            Speak(questionText ,this.Tts);

            //this.StartRecognizing();
        }
    }

    /**
     * Asks a confirmation.
     * @param {string} questionText 
     * @param {function} callback 
     * @param  {...any} params 
     */
    AskConfirmation = (questionText, callback, ...params) => {
        if(!this.isInConfirmation)
        {
            this.questionInfo = {
                callback: callback,
                question: questionText,
                params: params,
                isConfirmed: false
            }
            
            this.isInConfirmation = true;

            Speak(questionText ,this.Tts);
        }
    }

    /**
     * Process the command given by the user.
     * @param {string[]} words 
     */
    RecognizeCommand = (words) =>{
        var recognized = false;
        var status = this.applicationState.status;
        var self = this;
        
        //If is in a route, only expects to finish the route
        //TODO: Organizar essa função.
        if(RouteService.IsInRoute())
        {
            for(var i = 0; i< words.length; i++)
            {
                var word = words[i].toLowerCase();
                if(word == "finalizar rota" || word == "sair rota")
                {
                  RouteService.ClearRoute();
                  Speak('rota finalizada',this.Tts);
                  recognized = true;
                  return;
                }
            }
            Speak('comando de rota não reconhecido',this.Tts);
            return;
        }

        for(var i = 0; i< words.length; i++)
        {
          console.log(words[i]);
          var word = words[i].toLowerCase();
          if(word == 'semaforo' || word == 'semáforo')
          {
            StatusText(status,this.Tts);
            recognized = true;
            return;
          }
          else if(word == 'teste')
          {
            Speak('Oi, estou funcionando!',this.Tts);
            recognized = true;
            return;
          }
          else if(word == "próximo" || word == "proximo" || word == "proximidade")
          {
            NearbyPlaces(this.location.latitude, this.location.longitude, this.Tts);
            recognized = true;
            return;
          }
          else if((word.includes('proximos') || word.includes('próximos')) && word.split(' ').length == 2)
          {
            keyword = word.split(' ')[0];
            NearbyPlaces(this.location.latitude, this.location.longitude, this.Tts, keyword);
            recognized = true;
            return;
          }
          else if(word == "minha localização" || word == "localização")
          {
            LocationAddress(this.location.latitude, this.location.longitude, this.Tts);
            recognized = true;
            return;
          }
          else if(word == "salvar minha localização" || word == "salvar meu local")
          {
            self.AskQuestion('Informe um nome para a localização' , 'Confirma o nome ', InsertUserLocation,this.location.latitude, this.location.longitude);
            recognized = true;
            return;
          }
          else if(word == "remover localização" || word == "remover local")
          {
            self.AskQuestion('Qual o nome do local que deseja remover?' , 'Confirma o nome ', RemoveUserLocation);
            recognized = true;
            return;
          }
          else if(word == "informações rota" || word == "minha rota")
          {
            RouteService.RouteInformation(this.Tts);
            recognized = true;
            return;
          }
          else if(word == "finalizar rota")
          {
            RouteService.ClearRoute();
            Speak('rota finalizada',this.Tts);
            recognized = true;
            return;
          }
          else if(word == "configuração atualização rota")
          {
            self.AskQuestion('Qual o tempo de atualização em segundos deseja receber as informações?' , 'Confirma o tempo em segundos igual a ', RouteService.SetRouteUpdateTimer);
            recognized = true;
            return;
          }
          else if(word.includes('ir para '))
          {
            var Tts = this.Tts;
            var location = this.location;
            var locationName = word.replace('ir para ', '');
            FindPlace(locationName,Tts).then(function(result){
                if(result != null)
                {
                    if(result.confirmLocation)
                    {
                        self.AskConfirmation('Confirma o local pesquisado ' + result.locationName , InitRoute, result.latitude, result.longitude, location.latitude, location.longitude, result.locationName);
                    }
                    else
                    {
                        InitRoute(result.latitude, result.longitude, location.latitude, location.longitude, result.locationName);
                    }
                }
            });
            recognized = true;
            return;
          }
        }
    
        if(recognized == false)
        {
          Speak('comando não reconhecido',this.Tts);
        }
    }
}

export default TalkBotService.getInstance();