import {
    GetRoute
} from '../api/MapBoxAPI.js';

import RouteService from '../service/RouteService.js';

import{Speak} from '../traffic_lights.js';

/**
 * Calculates a route using the mapbox api.
 * @param {string} locationLatitude 
 * @param {string} locationLongitude 
 * @param {string} destinationLatitude 
 * @param {string} destinationLongitude 
 * @param {string} locationName 
 * @returns 
 */
export async function InitRoute(locationLatitude, locationLongitude, destinationLatitude, destinationLongitude, locationName)
{
    //Speak('traçando rota para ' + locationName,Tts);
    //Tts.speak(, { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });

    var route = await GetRoute(locationLatitude, locationLongitude, destinationLatitude, destinationLongitude);

    if(route == null || route.routes == null || route.routes[0].geometry == null || route.routes[0].geometry.coordinates == null)
    {
        console.log("Erro ao encontrar rota");
    }
    else
    {
        return RouteService.GenerateRouteObject(route.routes[0].geometry.coordinates);
    }
}

