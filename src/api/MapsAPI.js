const MAPS_KEY = "googleMapsKey"


/**
 * Get all points of interest in a 30 meter radius.
 * @param {*} latitude GPS coordinates
 * @param {*} longitude GPS coordinates
 * @param {*} keyword Keyword to search
 */
export async function GetPointsOfInterest(latitude, longitude, keyword){
    try{
        let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?"
        
        url += "&radius=10&key=" + MAPS_KEY + "&type=point_of_interest"
        url += "&location=" + latitude + "," + longitude

        if(keyword != null && keyword != '')
        {
            url += "&keyword=" + keyword;
        }
        
        let response = await fetch(url);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        //Alert.alert("Debug api",'retorno: ' + stringResponse);

        console.log('APIMAPS: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error MapsAPI(GetStatus)');
        return null;
    }
}

/**
 * Get the addresses nearby.
 * @param {*} latitude GPS coordinates
 * @param {*} longitude GPS coordinates
 */
export async function GetLocationAddress(latitude, longitude)
{
    try{
        let url = "https://maps.googleapis.com/maps/api/geocode/json?"
        
        url += "&key=" + MAPS_KEY 
        url += "&latlng=" + latitude + "," + longitude
        
        let response = await fetch(url);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        //Alert.alert("Debug api",'retorno: ' + stringResponse);

        console.log('APIMAPS: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error MapsAPI(GetStatus)');
        return null;
    }
}

/**
 * Get the addresses from a text.
 * @param {string} text GPS coordinates
 */
export async function GetAddressFromText(text)
{
    try{
        let url = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?inputtype=textquery&fields=formatted_address,geometry,name&key=" + MAPS_KEY 
        
        url += "&input=" + text
        
        let response = await fetch(url);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        //Alert.alert("Debug api",'retorno: ' + stringResponse);

        console.log('APIMAPS: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error MapsAPI(GetStatus)');
        return null;
    }
}