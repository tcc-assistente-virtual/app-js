const MAPS_KEY = "mapBoxKey"

/**
 * Calculate a route between two points using MapBoxAPI.
 * @param {decimal} locationLatitude 
 * @param {decimal} locationLongitude 
 * @param {decimal} destinationLatitude 
 * @param {decimal} destinationLongitude 
 */
export async function GetRoute(locationLatitude, locationLongitude, destinationLatitude, destinationLongitude)
{

    try{
        let url = "https://api.mapbox.com/directions/v5/mapbox/walking/" + locationLongitude + "," + locationLatitude + ";" + destinationLongitude +  "," + destinationLatitude ;
        url +=  "?alternatives=true&geometries=geojson&steps=false&access_token=" + MAPS_KEY; 
        console.log(url);
        let response = await fetch(url);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        console.log('APIMAPBOX: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Erro MapBoxAPI(GetRoute)' +  JSON.stringify(error));
        return null;
    }
}