import { Alert } from "react-native";

const serverIP = 'http://192.168.1.3';

/**
 * Get traffic light status.
 * @param {integer} trafficLightId 
 */
export async function GetStatus(trafficLightId) {
    try{
        let response = await fetch(serverIP+'/trafficlight/'+trafficLightId);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        //Alert.alert("Debug api",'retorno: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error API(GetStatus)');
        return null;
    }
}

/**
 * Finds the closest traffic light given a position.
 * @param {decimal} latitude 
 * @param {decimal} longitude 
 */
export async function GetClosestLight(latitude,longitude, heading)
{
    try{
        let response = await fetch(serverIP+'/trafficlight/'+latitude+'/'+longitude + '/' + heading);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        //Alert.alert("Debug api",'retorno: ' + stringResponse);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error API(GetClosestLight)');
        return null;
    }
    
}

/**
 * Searchs the database for a location text.
 * @param {string} text 
 */
export async function GetLocalAddressFromText(text)
{
    try{
        let response = await fetch(serverIP+'/location/search/'+text);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error API(GetLocalAddressFromText)');
        return null;
    }
}


/**
 * Searches nerby places.
 * @param {decimal} latitude 
 * @param {decimal} longitude 
 */
export async function GetPointsOfInterestByLocation(latitude, longitude)
{
    try{
        let response = await fetch(serverIP+'/location/points-of-interest/'+latitude+'/'+longitude);
        let data = await response.json();
        var stringResponse = JSON.stringify(data);
        return JSON.parse(stringResponse);
    }catch(error){
        console.log('Error API(GetPointsOfInterest)');
        return null;
    }
}

/**
 * Searches nerby places.
 * @param {decimal} latitude 
 * @param {decimal} longitude 
 * @param {string} type 
 */
 export async function GetPointsOfInterestByType(latitude, longitude, type)
 {
     try{
         let response = await fetch(serverIP+'/location/points-of-interest-by-type/'+latitude+'/'+longitude + '/' + type);
         let data = await response.json();
         var stringResponse = JSON.stringify(data);
         return JSON.parse(stringResponse);
     }catch(error){
         console.log('Error API(GetPointsOfInterestByType)');
         return null;
     }
 }