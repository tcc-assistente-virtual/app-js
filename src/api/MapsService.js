import {
    GetPointsOfInterest,GetLocationAddress, GetAddressFromText
} from '../api/MapsAPI.js';

import {
    GetLocalAddressFromText, GetPointsOfInterestByLocation, GetPointsOfInterestByType
} from '../api/api.js';

import {
    GetUserInformation
} from '../service/UserService.js';

/**
 * Finds nearby places using google api.
 * @param {string} latitude 
 * @param {string} longitude 
 * @param {*} Tts
 * @param {*} keyword  
 */
export async function NearbyPlaces(latitude, longitude, Tts, keyword)
{
    var str = "";
    var points = null;

    if(keyword != null && keyword != '')
    {
        points = await GetPointsOfInterestByType(latitude, longitude, keyword);
    }
    else
    {
        points = await GetPointsOfInterestByLocation(latitude, longitude);
    }

    if(points != null && points.length > 0)
    {
        var totalPlaces = 3;

        for(var i = 0; i < totalPlaces && i < points.length ; i ++) //Take only 3 places
        {
            console.log(points[i].locationname)

            Speak("lugar encontrado " + points[i].locationname + " ", Tts);
        }
    }
    else
    {
        points = await GetPointsOfInterest(latitude, longitude, keyword);

        if(points != null && points.results.length > 0)
        {
            var totalPlaces = 3;
            if(keyword != null && keyword != '')
            {
                totalPlaces = points.results.length;
            }
    
            for(var i = 0; i < totalPlaces && i < points.results.length ; i ++) //Take only 3 places
            {
                console.log(points.results[i].name)
    
                Speak("lugar encontrado " + points.results[i].name + " ", Tts);
            }
    
        }
        else
        {
            Speak("não encontrei nenhum local", Tts);
        }
    }
}

/**
 * Find the addres given the coordinates.
 * @param {string} latitude 
 * @param {string} longitude 
 * @param {*} Tts 
 */
export async function LocationAddress(latitude, longitude, Tts)
{
    var str = "";
    var points = await GetLocationAddress(latitude, longitude);

    
    if(points != null && points.results.length > 0)
    {
        console.log("Você está próximo a " + points.results[0].formatted_address + " ");
        Speak("Você está próximo a " + points.results[0].formatted_address + " ", Tts);

    }
    else
    {
        Speak("não encontrei nada", Tts);
    }

}

/**
 * Finds a place using the local database and the google maps api.
 * @param {string} placeName 
 * @param {*} Tts 
 * @returns 
 */
export async function FindPlace(placeName, Tts)
{
    //Search user local database:
    var userInformation = await GetUserInformation();

    console.log('-');
    console.log(JSON.stringify(userInformation));

    if(userInformation != null && userInformation.userLocations != null && userInformation.userLocations.length > 0)
    {
        for(var i = 0; i < userInformation.userLocations.length;i++)
        {
            if(userInformation.userLocations[i].locationName.toLowerCase().includes(placeName.toLowerCase))
            {
                //Speak("traçando rota para " + userInformation.userLocations[i].locationName, Tts);
                var result = { 
                    latitude: userInformation.userLocations[i].locationLatitude, 
                    longitude: userInformation.userLocations[i].locationLongitude,
                    locationName: userInformation.userLocations[i].locationName,
                    confirmLocation: false
                };

                return result;
            }
        }
    }

    //Search api:
    var responseApi = await GetLocalAddressFromText(placeName);

    if(responseApi != null && responseApi.length > 0)
    {
        console.log(responseApi);
        var result = {
            latitude: responseApi[0].latitude, 
            longitude: responseApi[0].longitude,
            locationName: responseApi[0].locationname, 
            confirmLocation: true
        }

        

        return result;
    }

    //Search google maps api: 
    var response = await GetAddressFromText(placeName);

    if(response != null && response.candidates != null && response.candidates.length > 0)
    {
        var result = {
            latitude: response.candidates[0].geometry.location.lat, 
            longitude: response.candidates[0].geometry.location.lng, 
            locationName: response.candidates[0].name, 
            confirmLocation: true
        }

        console.log(result);

        return result;
    }

    Speak("não encontrei nenhum local, tente ser mais específico.", Tts);

    return null;
}

function Speak(text, Tts)
{
  Tts.speak(text, { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });
}

/**
 * Normalizes the received object from google api.
 * @param {object} point 
 */
function NormalizePointOfInterest(point, type)
{
    
}