

/**
 * Calculates the distance between two points
 * @param {float} lat1 
 * @param {float} lon1 
 * @param {float} lat2 
 * @param {float} lon2 
 */
export function CalculateDistance(lat1, lon1, lat2, lon2) {
    var earthRadiusKm = 6371;
  
    var dLat = degreesToRadians(lat2-lat1);
    var dLon = degreesToRadians(lon2-lon1);
  
    lat1 = degreesToRadians(lat1);
    lat2 = degreesToRadians(lat2);
  
    var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
            Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    return earthRadiusKm * c;
}

function degreesToRadians(degrees) {
    return degrees * Math.PI / 180;
}
   
// Converts from radians to degrees.
function toDegrees(radians) {
    return radians * 180 / Math.PI;
}
  
/**
 * Calculates the bearing given two points.
 * @param {float} startLat 
 * @param {float} startLng 
 * @param {float} destLat 
 * @param {float} destLng 
 */
export function Bearing(startLat, startLng, destLat, destLng){
    startLat = degreesToRadians(startLat);
    startLng = degreesToRadians(startLng);
    destLat = degreesToRadians(destLat);
    destLng = degreesToRadians(destLng);
  
    y = Math.sin(destLng - startLng) * Math.cos(destLat);
    x = Math.cos(startLat) * Math.sin(destLat) -
          Math.sin(startLat) * Math.cos(destLat) * Math.cos(destLng - startLng);
    brng = Math.atan2(y, x);
    brng = toDegrees(brng);
    return (brng + 360) % 360;
}

/**
 * Calculates the heading(h) to a point, considering the heading of the device.
 * @param {*} heading 
 * @param {*} bearing 
 */
export function RelativeBearing(heading, bearing)
{
    var h = (heading - bearing) * -1;
    return (h + 360) % 360;
}

/**
 * Translates a heading to a direction.
 * @param {*} heading 
 */
export function HeadingToText(heading)
{
    if(heading >= 350 || heading <= 10)
        return "a frente";
    if(heading > 10 && heading <= 50)
        return "levemente a direita";
    else if(heading > 50 && heading <= 80)
        return "acentuada a direita";
    else if(heading > 80 && heading <= 100)
        return "a direita";
    else if(heading > 100 && heading <= 260)
        return "direção contrária"
    else if(heading > 260 && heading <= 280)
        return "a esquerda"
    else if(heading > 280 && heading <= 310)
        return "acentuada a esquerda"
    else if(heading > 310 && heading <= 350)
        return "levemente a esquerda"
    else
        return "erro ao encontrar direcao";
}