
function StatusToColor(status)
{
  if(status == 0)
  {
    return "#c40000";
  }
  else if(status == 1)
  {
    return "#21c400";
  }
  else if(status == 2)
  {
    return "#f0e402";
  }
  else
  {
    return "#000000";
  }
}

export function LightToDisplay(status,light)
{
  if(status == light) //|| status == 2 && light == 0)
  {
    return StatusToColor(status);
  }
  else
  {
    return "#000000";
  }
}

/**
 * Translates the status of the traffic light and outputs to text to speech.
 * @param {int} status 
 * @param {*} Tts 
 */
export function StatusText(status, Tts)
{
  if(status == null) Tts.speak("nenhum semáforo encontrado", { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });
  else if(status == 0) Tts.speak("seguro atravessar", { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });
  else Tts.speak("não atravessar", { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });
}

/**
 * Translates the status of the traffic light.
 * @param {int} status 
 * @returns 
 */
export function Status(status)
{
  if(status==null) return "";
  else if(status == 0) return "seguro atravessar";
  else return "não atravessar";
}

export function Speak(text, Tts)
{
  Tts.speak(text, { androidParams: { KEY_PARAM_STREAM: 'STREAM_MUSIC' } });
}