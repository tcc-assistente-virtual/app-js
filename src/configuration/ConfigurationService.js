import{StoreLocalObject, GetLocalObject} from '../repository/LocalRepository.js';

//User Preferences
/**
 * Gets a user preference given a key.
 * @param {string} key 
 * @returns 
 */
 export async function GetUserPreference(key)
 {
     return await GetLocalObject(key);
 }
 
 /**
  * Sets a user preference given a key.
  * @param {string} key 
  * @param {string} value 
  * @returns 
  */
 export function SetUserPreference(key, value)
 {
     return StoreLocalObject(key, value);
 }