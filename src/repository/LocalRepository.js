import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * Stores a string in the local repository.
 * @param {string} key 
 * @param {string} value 
 */
export async function StoreLocalData(key, value)
{
    try {
        await AsyncStorage.setItem(key, value);
      } catch (e) {
        // saving error
        console.log('erro ao salvar: ' + e);
      }
}

/**
 * Retrieves a string from the local repository.
 * @param {string} key 
 */
export async function GetLocalData(key)
{
    try {
        const value = await AsyncStorage.getItem(key);
        if(value !== null) {
          return value;
        }

        return null;
      } catch(e) {
        // error reading value
        console.log('erro ao buscar: ' + e);
        return null;
      }
}

/**
 * Stores an object in the local repository.
 * @param {string} key 
 * @param {object} object 
 */
export async function StoreLocalObject(key, object)
{
    try {
        const jsonValue = JSON.stringify(object);
        await AsyncStorage.setItem(key, jsonValue);
        return true;
      } catch (e) {
        // saving error
        console.log('erro ao salvar: ' + e);
        return false;
      }
}

/**
 * Retrieves an object from the local repository.
 * @param {string} key 
 */
export async function GetLocalObject(key)
{
    try {
        const jsonValue = await AsyncStorage.getItem(key);
        return jsonValue != null ? JSON.parse(jsonValue) : null;
      } catch(e) {
        // error reading value
        console.log('erro ao buscar: ' + e);
        return null;
      }
}


