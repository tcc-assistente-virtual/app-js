import{Speak} from '../utils.js';
import{GetClosestLight, GetStatus} from '../api/api.js';
import{CalculateDistance} from '../geolocation.js'

class TrafficLightService{

    status = null;
    trafficLightId = null;
    latitude = null;
    longitude = null;
    api = null;

    static instance = null;

    static getInstance() {

        if (!TrafficLightService.instance) {

            TrafficLightService.instance = new TrafficLightService();

        }
        return TrafficLightService.instance;
    }

    IsConnected(){
        if(this.status != null) return true;
        else return false;
    }

    StatusToColor()
    {
        if(this.status == 0)
        {
            return "#c40000";
        }
        else if(this.status == 1)
        {
            return "#21c400";
        }
        else if(this.status == 2)
        {
            return "#f0e402";
        }
        else
        {
            return "#000000";
        }
    }

    LightToDisplay(light)
    {
        if(this.status == light) //|| status == 2 && light == 0)
        {
            return this.StatusToColor(this.status);
        }
        else
        {
            return "#000000";
        }
    }

    StatusText(Tts)
    {
        
        if(this.status == null) Speak("nenhum semáforo encontrado", Tts);
        else if(this.status == 0) Speak("seguro atravessar", Tts);
        else Speak("não atravessar", Tts);
    }

    Status()
    {
        if(this.status == null) return "";
        else if(this.status == 0) return "seguro atravessar semáforo";
        else return "não é seguro atravessar semáforo";
    }

    StatusNumber(){
        return this.status;
    }

    ConnectTrafficLight(location){
        GetClosestLight(location.latitude, location.longitude, location.heading).then(data => 
        {
            console.log('[ConnectTrafficLight] ' + data);
            if(data != null)
            {
                this.trafficLightId = data.trafficlightid;
                this.latitude = data.latitude;
                this.longitude = data.longitude;
                this.status = data.status;
                this.api = true;
            }
            else
            {
                this.trafficLightId = null;
                this.latitude = null;
                this.longitude = null;
                this.status = null;
                this.api = null;
            }
        });

       
    }

    UpdateTrafficLight(location){
        //Verify distance
        var distance = CalculateDistance(this.latitude, this.longitude, location.latitude, location.longitude);

        console.log('[Traffic Light]: Distance = ' + distance);

        if(distance > 0.07)
        {
            this.trafficLightId = null;
            this.latitude = null;
            this.longitude = null;
            this.status = null;
            this.api = null;
        }
        else
        {
          //Update from API
          GetStatus(this.trafficLightId).then(data => 
            {  
              if(data == null){
                this.trafficLightId = null;
                this.latitude = null;
                this.longitude = null;
                this.status = null;
                this.api = null;
              }else{
                this.status = data[0].status;
              }
            });
        }

        console.log('[UpdateTrafficLight] ' + this.status);
    }

    UpdateTrafficLightMQTT(result, location){
        //Verify distance
        if(CalculateDistance(result.latitude, result.longitude, location.latitude, location.longitude) > 0.5)
        {
            this.trafficLightId = null;
            this.latitude = null;
            this.longitude = null;
            this.status = null;
            this.api = null;
        }
        else
        {
            this.trafficLightId = result.id;
            this.latitude = result.latitude;
            this.longitude =result.longitude;
            this.status = result.status.toString();
            this.api = false;
        }
    }

    Disconnect(){
        if(this.api == false)
        {
            this.trafficLightId = null;
            this.latitude = null;
            this.longitude = null;
            this.status = null;
            this.api = null;
        }
    }
}

export default TrafficLightService.getInstance();