import {
    CalculateDistance, Bearing, RelativeBearing, HeadingToText
} from '../geolocation.js';

import {
    GetUserPreference, SetUserPreference
} from '../configuration/ConfigurationService';

import{Speak} from '../traffic_lights.js';

import TrafficLightService  from './TrafficLightService.js'

class RouteService{
    
    route = {
        completed: false,
        message: "", 
        distanceToPoint: 0,
        bearingToPoint: 0,
        relativeBearing: 0,
        pointIndex: 0,
        points: []
    };

    Tts = null;

    static instance = null;

    static getInstance() {

        if (!RouteService.instance) {

            RouteService.instance = new RouteService();

        }
        return RouteService.instance;
    }

    Register = (Tts) =>
    {
        this.Tts = Tts;
    }

    /**
     * Clears the current route.
     */
    ClearRoute = () =>
    {
        console.log('Rota foi limpa');
        this.route = {
            completed: false,
            message: "", 
            distanceToPoint: 0,
            bearingToPoint: 0,
            relativeBearing: 0,
            pointIndex: 0,
            points: []
        };
    }

    /**
     * Finishes the current route.
     */
    CompleteRoute = () =>
    {
        this.route = {
            completed: true,
            message: "Você chegou ao seu destino", 
            distanceToPoint: 0,
            bearingToPoint: 0,
            relativeBearing: 0,
            pointIndex: -1,
            points: this.route.points
        };
    }

    /**
     * Generates a route object.
     * @param {*} coordinates 
     */
    GenerateRouteObject = (coordinates) =>
    {
        this.route = {
            completed: false,
            message: "", 
            distanceToPoint: 0,
            bearingToPoint: 0,
            relativeBearing: 0,
            pointIndex: 0,
            points: []
        };

        for(var i = 0; i < coordinates.length; i++)
        {
            this.route.points[i] = {
                order: i,
                coordinate: coordinates[i],
                checkpoint: false
            }
        }

        console.log('route generated: '+ JSON.stringify(this.route));

        Speak('rota iniciada',this.Tts);
        return this.route;
    }

    /**
     * Refresh the current checkpoint based in a user location.
     * @param {*} location 
     * @param {integer} index 
     */
    UpdateDestinationCheckpoint = (location, index) =>
    {
        var distance = CalculateDistance(this.route.points[index].coordinate[1],this.route.points[index].coordinate[0], location.latitude, location.longitude);
        var bearing = Bearing( location.latitude, location.longitude,this.route.points[index].coordinate[1],this.route.points[index].coordinate[0]);

        var relativeBearingCalculated = RelativeBearing(location.heading, bearing);

        this.route = {
            completed: false,
            message: HeadingToText(relativeBearingCalculated), 
            distanceToPoint: distance*1000,
            bearingToPoint: bearing,
            relativeBearing: relativeBearingCalculated,
            pointIndex: index,
            points: this.route.points
        };
    }

    /**
     * Refresh the current route based in a user location.
     * @param {*} location 
     */
    UpdateRoute = (location) =>
    {
        if(this.route == null || this.route.points.length <= 0 || this.route.completed == true)
        {
            console.log('não deu update na rota');
            return this.route;
        } 

        if(this.route.points.some(f => !f.checkpoint)) //If has at least one checkpoint to be reached
        {
            for(var i = 0; i < this.route.points.length; i++) //finds the first checkpoint
            {
                if(!this.route.points[i].checkpoint)
                {
                    var distance = CalculateDistance(this.route.points[i].coordinate[1],this.route.points[i].coordinate[0], location.latitude, location.longitude);

                    if(distance*1000 <= 2)//reached the checkpoint
                    {
                        this.route.points[i].checkpoint = true;

                        if(i + 1 < this.route.points.length)
                        {
                            this.UpdateDestinationCheckpoint(location, i+1);
                        }
                        else
                        {
                            this.CompleteRoute();
                        }
                    }
                    else
                    {
                        this.UpdateDestinationCheckpoint(location, i);
                    }
                }
            }
        }
        else
        {
            this.CompleteRoute();
        }

        return this.route;
    }

    /**
     * Returns the directions of the current route.
     */
    RouteInformation()
    {
        if(this.route == null || this.route.points == null || this.route.points.length == 0)
        {
            Speak('nenhuma rota encontrada',this.Tts);
        }
        else
        {
            var message = this.route.message;
            var text = TrafficLightService.Status();

            if(text != null && text != '')
            {
                message = message + ' e ' + text; 
            }

            Speak(message, this.Tts);
        }
    }

    /**
     * Returns the directions of the current route.
     * If there are no active route does not return anything.
     */
     RouteInformationUpdate()
     {
         if(this.route != null && this.route.points != null && this.route.points.length > 0)
         {
            var message = this.route.message;
            var text = TrafficLightService.Status();

            if(text != null && text != '')
            {
                message = message + ' e ' + text; 
            }

            console.log(this.route.message + " ->" + message);

            Speak(message, this.Tts);
         }
     }

     /**
      * Indicates if there is an active route.
      * @returns 
      */
     IsInRoute()
     {
        if(this.route != null && this.route.points != null && this.route.points.length > 0)
        {
           return true;
        }

        return false;
     }

     /**
      * Gets the route update timer.
      * @returns 
      */
     GetRouteUpdateTimer()
     {
        var timer = 5000;

        var timer = GetUserPreference('@routeUpdateTimer');

        if(timer == null || timer < 0)
        {
            return 5000;
        }

        return timer;
     }

     /**
      * Sets the update timer
      * @param {int} value in seconds
      * @returns 
      */
     SetRouteUpdateTimer(value)
     {
        if(value != null && parseInt(value, 10) > 0)
        {
            //Convert to miliseconds
            var miliseconds = Math.ceil(value * 1000);
            if(SetUserPreference('@routeUpdateTimer', miliseconds))
            {
                Speak('A configuração foi alterada, o aplicativo precisa ser reiniciado para que a atualização tenha efeito.', Tts);
                return true;
            }
                
        }

        Speak('O valor informado não foi reconhecido.', this.Tts);
        return false;
     }
}

export default RouteService.getInstance();