import{StoreLocalObject, GetLocalObject} from '../repository/LocalRepository.js';

/**
 * Retrieves the user information from the local database.
 */
export async function GetUserInformation()
{
    return await GetLocalObject('@userInformation');
}

/**
 * Insert the user data with a location in the local database.
 * @param {string} userName 
 * @param {*} location 
 */
export function InsertUserInformation(userName, location)
{
    userInformation = {
        userName: userName,
        userLocations: [
          {
            locationName: location.locationName,
            locationLatitude: location.locationLatitude,
            locationLongitude: location.locationLongitude,
          }
        ]
    }

    return StoreLocalObject('@userInformation', userInformation);
}

/**
 * Insert the user data in the local database.
 * @param {string} userName 
 */
export function InsertUserInformationName(userName)
{
    userInformation = {
        userName: userName,
        userLocations: []
    }

    return StoreLocalObject('@userInformation', userInformation);
}

/**
 * Checks if user is already registered.
 */
export async function IsUserRegistered()
{
    
    var user = await GetLocalObject('@userInformation');
    if(user == null || user.userName == null || user.userName == "")
    {
        return false;
    }

    return true;
}

/**
 * Stores a location in the user's local database.
 * @param {string} locationName 
 * @param {decimal} latitude 
 * @param {decimal} longitude 
 */
export function InsertUserLocation(locationName, latitude, longitude)
{
    GetLocalObject('@userInformation').then(function(user) {
        //console.log('COnsulta user para salvar local ' + JSON.stringify(user));

        if(user == null || user.userName == null || user.userName == "")
        {
            return false;
        }

        location = {
            locationName: locationName,
            locationLatitude: latitude,
            locationLongitude: longitude,
        }

        user.userLocations.push(location);

        console.log('localização para ser salva: ' + JSON.stringify(user));

        return StoreLocalObject('@userInformation', user);
    });
}

/**
 * Removes a location from the user's local database.
 * @param {string} locationName 
 */
export function RemoveUserLocation(locationName)
{
    GetLocalObject('@userInformation').then(function(user) {

        if(user == null || user.userName == null || user.userName == "")
        {
            return false;
        }

        var index = -1;
        for(var i = 0; i< user.userLocations.length ; i++)
        {
            if(user.userLocations[i].locationName.toLowerCase() == locationName.toLowerCase())
            {
                index = i;
            }
        }

        if(index > 1)
        {
            
            user.userLocations.splice(index, 1);
        }
        else
        {
            return false;
        }

        return StoreLocalObject('@userInformation', user);
    });
}

