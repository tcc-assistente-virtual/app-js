**Assistente Virtual**

Aplicativo com a assistente virtual feito em React Native.

## Watchman

Erro watchman não carrega/stuck on dependency graph:

```
echo 999999 | sudo tee -a /proc/sys/fs/inotify/max_user_watches && echo 999999 | sudo tee -a /proc/sys/fs/inotify/max_queued_events && echo 999999 | sudo tee -a /proc/sys/fs/inotify/max_user_instances && watchman shutdown-server && sudo sysctl -p
```

## Iniciar emulator android studio

```
sudo chown username -R /dev/kvm
```

```
emulator -list-avds

emulator -avd "Nexus_5X_API_29"

adb shell input keyevent 82
```

## GERAR APK

```
react-native bundle --dev false --platform android --entry-file index.js --bundle-output ./android/app/src/main/assets/index.android.bundle --assets-dest ./android/app/src/main/res
```

```
$ cd android
#Create debug build:
$ ./gradlew assembleDebug
#Create release build:
$ ./gradlew assembleRelease #Generated `apk` will be located at `android/app/build/outputs/apk`
```

## Redirecionar udp:
https://iomem.com/archives/23-Redirecting-TCP-and-UDP-traffic-to-the-Android-emulator.html

## Habilitar websockets no mosquitto:

Seguir o tutorial: https://gist.github.com/smoofit/dafa493aec8d41ea057370dbfde3f3fc

abrir o .conf e adicionar em extra:

```
listener 8098
protocol websockets
socket_domain ipv4
```
Para iniciar o mosquitto:

```
mosquitto -c /etc/mosquitto/mosquitto.conf
```
## CONFIG RELEASE
```
react-native run-android --variant release #android

react-native run-ios --configuration Release #ios
```

