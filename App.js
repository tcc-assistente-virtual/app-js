/**
 * App
 *
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  Button,
  StatusBar,
  Alert,
  FlatList,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';

import { List, ListItem } from 'react-native-elements'

import Geolocation from 'react-native-geolocation-service';
import {CalculateDistance, HeadingToText} from "./src/geolocation.js";

import Tts from 'react-native-tts';

import Zeroconf from 'react-native-zeroconf';

import RNShake from 'react-native-shake';

import Voice, {
  SpeechRecognizedEvent,
  SpeechResultsEvent,
  SpeechErrorEvent,
} from '@react-native-community/voice';

import MqttService from "./src/mqtt/MqttService";
import TalkBotService from './src/talkbot/TalkBotService.js'
import TrafficLightService from './src/service/TrafficLightService.js'
import RouteService from './src/service/RouteService.js';
import {IsUserRegistered, InsertUserInformationName} from './src/service/UserService.js';

const zeroconf = new Zeroconf()

export default class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      mqttActive: false, 
      location: '', //Location of the user
      isScanning: false, //Scanning DNS SD
      selectedService: null, 
      services: {}, //MQTT services in the local network
      interval: null, //executes every 3000ms
      routeInterval: null,
      userInformation: {
        userName: "",
        userLocations: [
          {
            locationName: "",
            locationLatitude: '',
            locationLongitude: ''
          }
        ]
      },
      routeGps: {
        completed: false,
        message: "", 
        distanceToPoint: 0,
        bearingToPoint: 0,
        relativeBearing: 0,
        pointIndex: 0,
        points: []
      },
    }

    Tts.setDefaultLanguage('pt-BR');

    //voice
    TalkBotService.register(Tts, Voice, this.state);
    RouteService.Register(Tts);
    RouteService.ClearRoute();
  }

  componentDidMount(){
    
    // Timer route
    // var self = this;
    RouteService.GetRouteUpdateTimer().then(function(result)
    {
      var routeInterval = setInterval(() => {
        console.log('[Route Info Start]')
        RouteService.RouteInformationUpdate(Tts);
      }, result);
  
      self.setState({routeInterval: routeInterval});
    });
    
    // Timer traffic light 
    var interval = setInterval(() => {
      console.log('[Refresh traffic lights]')
      const host = this.getHost();

      if(this.state.mqttActive == false)
      {
        //MQTT is not active.
        if(host != null && host != ''){//found a mqtt server in the local network
          MqttService.connectClient(this.mqttSuccessHandler,this.mqttConnectionLostHandler,host);
        }
        else if(!TrafficLightService.IsConnected()){//if is not connected to any traffic light, call api o get the closer one
          TrafficLightService.ConnectTrafficLight(this.state.location);
        }
        else
        {
          TrafficLightService.UpdateTrafficLight(this.state.location);
        }      
      }
    }, 3000);
    
    // store intervalId in the state so it can be accessed later:
    this.setState({interval: interval});

    this.refreshData();

    // //GPS
    Geolocation.watchPosition(
        (position) => {
          console.log('[GPS] ' + JSON.stringify(position));
          TalkBotService.setLocation(position.coords);
          this.setState({location: position.coords});
          var updatedRoute = RouteService.UpdateRoute(position.coords);
          console.log('[Route] Updated route: ' + JSON.stringify(updatedRoute))
          this.setState({routeGps: updatedRoute});
        },
        (error) => {
            // See error code charts below.
            Alert.alert("GPS","erro:" + error.message);
            console.log(error.code, error.message);
        },
        {enableHighAccuracy: true, timeout: 1500, maximumAge: 1000, distanceFilter: 1}
    );

  
    //zeroconf
    zeroconf.on('start', () => {
      this.setState({ isScanning: true })
      console.log('[Start zeroconf]')
    })

    zeroconf.on('stop', () => {
      this.setState({ isScanning: false })
      console.log('[Stop zeroconf]')
    })

    zeroconf.on('resolved', service => {
      console.log('[Resolve zeroconf]', JSON.stringify(service, null, 2))

      this.setState({
        services: {
          ...this.state.services,
          [service.host]: service,
        },
      })
    })

    zeroconf.on('error', err => {
      this.setState({ isScanning: false })
      console.log('[Error zeroconf]', err)
    });

    //Shake
    RNShake.addEventListener('ShakeEvent', () => {
      console.log('[Shake] Device was shaked.');
      this._startRecognizing();
    });


    //Verify registration
    // IsUserRegistered().then(function(result)
    // {
    //   if(!result)
    //   {
    //     console.log('[User] User is not registered.');
    //     TalkBotService.AskQuestion('Qual é o seu nome?' , 'Confirma o nome ', InsertUserInformationName);
    //   }
    //   else
    //   {
    //     console.log('[User] User is registered.');
    //   }
    // });
  }

  componentWillUnmount(){
    clearInterval(this.state.interval); //Clears the timer
   // clearInterval(this.state.intervalDNS); //Clears the timer

   Voice.destroy().then(Voice.removeAllListeners);//clears the voice listeners

   RNShake.removeEventListener('ShakeEvent');
  }


  _startRecognizing = async () => {

    try {
      await Voice.start('pt-BR');
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };

  _stopRecognizing = async () => {
    try {
      await Voice.stop();
    } catch (e) {
      //eslint-disable-next-line
      console.error(e);
    }
  };


  //Received a mqtt message:
  onMQTTReceived = messageReceived => {
    console.log('[MQTT]' + messageReceived);
    let result = JSON.parse(messageReceived);

    TrafficLightService.UpdateTrafficLightMQTT(result, this.location);
  }

  //Connected to mqtt:
  mqttSuccessHandler = () => {
    this.setState({mqttActive:true});

    //Subscribe to the topic
    MqttService.subscribe('/local', this.onMQTTReceived)
  };

  cross = () => {
    if(this.state.mqttActive == true)
    {
      MqttService.publishMessage('/crossing', '1')
    }
  }

  //MQTT disconected:
  mqttConnectionLostHandler = () => {
    TrafficLightService.Disconnect();
    this.setState({mqttActive:false});
  };

  //Scans the local network for mqtt services
  refreshData = () => {
    const { isScanning } = this.state
    if (isScanning) {
      return
    }

    this.setState({ services: [] })

    zeroconf.scan('mqtt', 'tcp', 'local.')

    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      zeroconf.stop()
    }, 5000)
  }

  //gets the first mqtt server found in the local newtork
  //TODO: Check if server is valid 
  getHost = () =>{
    const keys = Object.keys(this.state.services);

    if(keys.length > 0){
      return this.state.services[keys[0]].host + ':' + this.state.services[keys[0]].port;
    }
    else{
      return "";
    }
  }

  render() {

    return (
      <ScrollView >
        <Text style={{paddingTop:5, paddingLeft: 5}}>{'MQTT: ' + this.state.mqttActive}</Text>
        {/* <Text style={styles.label}>{'ID: ' + this.state.trafficLightId}</Text>
        <Text style={styles.label}>{'Location: ' + this.state.location.latitude + ',' + this.state.location.longitude}</Text>
        <Text style={styles.label}>{'Distance(KM): ' + CalculateDistance(parseFloat(this.state.location.latitude),parseFloat(this.state.location.longitude),parseFloat(this.state.latitude),parseFloat(this.state.longitude))}</Text> */}
        <Text style={styles.label}>{'Status Semáforo: ' + JSON.stringify(TrafficLightService.StatusNumber())}</Text>
        <Text style={styles.label}>{'HOST MQTT: ' + this.getHost()}</Text>
        <Text style={styles.label}>{'Rota: ' + this.state.routeGps.pointIndex + ' | DIST.: ' + this.state.routeGps.distanceToPoint + ' | BR.: ' + this.state.routeGps.bearingToPoint}</Text>
        <Text style={styles.label}>{'Direção: ' + this.state.routeGps.relativeBearing + ' | ' + HeadingToText(this.state.routeGps.relativeBearing)}</Text>
        
        {/* <View style={styles.button}>
             <Button
               title={"FORCE " + this.state.forceHTTP ? "MQTT" : "HTTP"}
               onPress={() => (this.toggleProtocol())}
             /> 
        </View> 
        <View style={styles.button}>
             <Button
               title="SPEAK"
               onPress={() => (StatusText(this.state.status,Tts))}
             /> 
        </View>
        <View style={styles.button}>
             <Button
               title="CROSS"
               onPress={() => (this.cross())}
             /> 
        </View>*/}
        <View style={styles.button}>
             <Button
               title="REFRESH SCAN (DNS)"
               onPress={() => (this.refreshData())}
             /> 
        </View>
        <View style={styles.button}>
             <Button
               title="Start recognizing"
               onPress={() => (TalkBotService.StartRecognizing())}
             /> 
        </View>
        <View style={styles.button}>
             <Button
               title="Stop recognizing"
               onPress={() => (this._stopRecognizing())}
             /> 
        </View>
        <View>
               <Text style={{fontSize: 40,textAlign: 'center',color: TrafficLightService.LightToDisplay(0)}}>
                 ●
               </Text>
               <Text style={{fontSize: 40,textAlign: 'center',lineHeight: 70,color: TrafficLightService.LightToDisplay(2)}}>
                 ●
               </Text>
               <Text style={{fontSize: 40,textAlign: 'center',lineHeight: 70,color: TrafficLightService.LightToDisplay(1)}}>
                 ●
               </Text>
              </View>
      </ScrollView>
    );
    }
}


const styles = StyleSheet.create({
  label:{
    paddingLeft: 5,
    paddingTop: 2
  },
  button:{
    paddingLeft: 20,
    paddingTop: 5,
    paddingRight: 20,
    paddingBottom:5,
    textAlign: 'center'
  },
})
